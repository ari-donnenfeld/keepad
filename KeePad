#!/bin/sh

# Overwrites previous line
overwrite() { echo -e "\r\033[1A\033[0K$@"; }

# Edit notes
# $1: note file
# $2: password
# $3: key file
edit_notes() {
    # Get notes
    overwrite "Decrypting..."
    old_notes=$(echo $2 | keepassxc-cli show -a notes $1 notes $3 2> /dev/null)

    # Check for incorrect password
    if [ $? -ne 0 ]; then
        overwrite "Incorrect Password."
        return
    fi

    # Edit notes
    notes=$(printf "$old_notes" | vipe)

    # Check for no change
    if [ "$notes" = "$old_notes" ]; then
        overwrite "No changes"
        exit 1
    fi

    overwrite "Saving..."
    # Save new notes
    echo $2 | keepassxc-cli edit --notes "$notes" $1 notes $3 > /dev/null 2>&1
    overwrite "Saved."
}

# Check if no arguments given or help
if [ -z "$1" ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
    echo "Usage: KeePad note [keyfile]"
    echo "Open or Create note file"
    echo ""
    echo "If note does not exist, it will be created"
    echo "If keyfile does not exist, it will be created"
elif [ -f "$1" ]; then # Check if file exists
    # Get password
    stty -echo
    printf "Password: "
    read pass
    stty echo
    printf "\n"

    # Check for key file
    if ! [ -z "$2" ]; then
        keyphrase="--key-file $2"
    fi

    edit_notes $1 $pass "$keyphrase"
else
    echo "File does not exist."
    echo "Enter password to create file or Control+C to cancel."
    stty -echo
    printf "Password: "
    read pass
    printf "\n"
    printf "Confirm Password: "
    read pass2
    stty echo
    printf "\n"

    # Check passwords match
    if ! [ $pass == $pass2 ]; then
        echo "Passwords do not match"
        exit 1
    fi

    printf "Decryption time in ms [1000 if empty]: "
    read dt

    # Check if dt is empty
    if [[ -z $dt ]]; then
        dt="1000"
    fi
    
    # Check if dt is a number
    if ! [[ $dt =~ ^[0-9]+$ ]]; then
        echo "Please enter an integer for decryption time"
        exit 1
    fi

    echo "Creating notes..."
    
    # Check for key file
    if ! [ -z "$2" ]; then
        keyphrase="--set-key-file $2"
    fi
    # Create database
    printf "$pass\n$pass" | keepassxc-cli db-create --set-password $1 --decryption-time $dt $keyphrase > /dev/null 2>&1

    # Check for key file
    if ! [ -z "$2" ]; then
        keyphrase="--key-file $2"
    fi

    # Add entry to database
    echo $pass | keepassxc-cli add $1 notes $keyphrase > /dev/null 2>&1

    edit_notes $1 $pass "$keyphrase"
fi
