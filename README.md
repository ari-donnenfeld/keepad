# KeePad

Super simple encrypted notepad.

Uses KeePass for its encryption so you don't have to worry about this program being abandoned.

## Usage

```
Usage: KeePad note [keyfile]
Open or Create note file

note: name or path of note file.
    If note does not exist, it will be created.
keyfile: (optional) name or path of key file.
    If keyfile does not exist, it will be created.
```

Set your preffered editor with the EDITOR environment variable.

e.g.
```bash
export EDITOR=/bin/vim
```

## Examples 

Create new note:

```bash
./KeePad note
```

```
File does not exist.
Enter password to create file or Control+C to cancel.
Password:
Confirm Password:
Decryption time in ms [1000 if empty]:
```

Edit note:
```bash
./KeePad note 
```

```
Password:
```

## Installation

```bash
git clone https://gitlab.com/ari-donnenfeld/keepad.git
cd keepad
chmod +x KeePad
cp -i KeePad $HOME/.local/bin
```

## Dependencies

- keepassxc-cli
- vipe (probably preinstalled)

## Program dissapear?

The note file is simply a KeePassXC database file.

If this program no longer works for you, simply install KeePassXC and open the notes file as the database, enter the password and keyfile as you did for KeePad. Your notes should be in the notes of the 'notes' entry.

